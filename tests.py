from uuid import uuid4

import pytest

from storerer.entity import Entity
from storerer.storage import PickleEngine


@pytest.fixture
def entity():
    data = {
        'name': 'kevin',
        'job': 'garbage',
        'number_of_children': -1,
        'dreamjob': '2pac',
        'id': str(uuid4())
    }        
    entity = Entity(**data)
    return entity, data


@pytest.fixture
def pickle_engine():
    return PickleEngine('/tmp/pickleaa')


def test_entity_assignment(entity):
    entity, data = entity
    
    assert dict(entity) == data


def test_getattr_override(entity):
    entity, data = entity

    for k, v in data.items():
        assert getattr(entity, k) == v
    assert entity.store_in.__class__.__name__ == 'method'


def test_setattr_override(entity):
    entity, data = entity

    for k, v in data.items():
        setattr(entity, k, 'new_value')
        assert getattr(entity, k) == 'new_value'


def test_storage(entity, pickle_engine):
    entity, data = entity

    entity.store_in(pickle_engine)

    entity.name = 'Cybertron'

    entity.get_from(pickle_engine)

    assert entity.name != 'Cybertron'
