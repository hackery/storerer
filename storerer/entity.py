

class Entity(object):

    id_key = 'id'

    def __init__(self, **kwargs):
        if self.id_key not in kwargs:
            kwargs[self.id_key] = None
        object.__setattr__(self, '_data', kwargs)

    def __getattr__(self, attr):
        try:
            return self._data[attr]
        except KeyError:
            raise AttributeError(attr)

    def __setattr__(self, attr, value):
        if attr in self._data:
            self._data[attr] = value
        else:
            super().__setattr__(attr, value)

    def __iter__(self):
        return iter(self._data.items())

    @property
    def identifier(self):
        return getattr(self, self.id_key)

    @identifier.setter
    def identifier(self, value):
        setattr(self, self.id_key, value)

    def store_in(self, storage_engine):
        self.id = storage_engine.store(
            dict(self), self.id_key
        )

    def get_from(self, storage_engine):
        self._data.update(
            storage_engine.get(self.id)
        )
