from .entity import Entity
from .storage import PickleEngine


e = Entity(x=1, y=2, z=3)
e.store_in(PickleEngine('/tmp/pickle.db'))


