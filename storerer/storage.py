import pickledb


class StorageEngine(object):

    auto_commit = True

    def store(self, data, identifier=None):
        raise NotImplemented

    def get(self, identifier):
        raise NotImplemented


class PickleEngine(StorageEngine):

    auto_commit = True

    def __init__(self, filename):
        self.db = pickledb.load(filename, False)

    def store(self, data, id_key):
        if not data[id_key]:
            raise ValueError('Identifier required')
        self.db.set(
            data[id_key],
            data
        )
        if self.auto_commit:
            self.db.dump()
        return data[id_key]

    def get(self, id_key):
        return self.db.get(id_key)
